<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $mysession = new Zend_Session_Namespace('mysession');
        if(!isset($mysession->Rol)){
            $this->redirect('/login/index/index');
        }
    }

    public function dashboardAction()
    {
        // action body
    }

    public function logoutAction()
    {
        Zend_Session::namespaceUnset('mysession');
        Zend_Session::destroy();
        $this->redirect('/login/index/index');
    }
    public function finalizarAction()
    {
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $Ticket= $this->getRequest()->getParam('ticket')  ;
        $Tickets= Model_Inbox::getFinalizar($Ticket);

        $data = array();
        foreach ($Tickets as $cat) {
            $idTicket= $cat["idticket"];
            $Fecha= $cat["fecha"];
            $Agente= $cat["agente"];
            $Nombre= $cat["nombres"];
            $Apellido= $cat["apellidos"];
            $Email= $cat["email"];
            $Llamada= $cat["idllamada"];
            $this->view->Ticket = $Ticket;
            $this->view->Llamada = $Llamada;
            $this->view->Email = $Email;
            $this->view->Fecha = $Fecha;
            $this->view->Agente = $Agente;
            $this->view->Nombres = $Nombre;
            $this->view->Apellidos = $Apellido;


        }
    }
    public function pasosAction()
    {
        $Llamada = $this->getRequest()->getParam('llamada')  ;
        $this->view->Llamada = $Llamada;
        $Ticket = $this->getRequest()->getParam('ticket')  ;
        $this->view->Ticket = $Ticket;

    }

    public function llamadaAction()
    {
        $Llamada = $this->getRequest()->getParam('llamada')  ;
        $this->view->Llamada = $Llamada;
        $Ticket = $this->getRequest()->getParam('ticket')  ;
        $this->view->Ticket = $Ticket;
    }

    public function crearTicketAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $Ticket = Model_Inbox::setTicket();
        $noTicket=0;
        foreach ($Ticket as $cat) {
            $noTicket=$cat["noticket"];
        }
        $Llamada = Model_Inbox::setLlamada($noTicket);
        $noLlamada=0;
        foreach ($Llamada as $cat) {
            $noLlamada=$cat["nollamada"];
        }

        Model_Inbox::updateTicketLlamada($noLlamada, $noTicket);
        $this->redirect('/admin/index/pasos/llamada/'.$noLlamada.'/ticket/'.$noTicket);
    }

    public function crearTicketsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $Ticket = Model_Inbox::setTicket();
        $noTicket=0;
        foreach ($Ticket as $cat) {
            $noTicket=$cat["noticket"];
        }
        $Llamada = Model_Inbox::setLlamada($noTicket);
        $noLlamada=0;
        foreach ($Llamada as $cat) {
            $noLlamada=$cat["nollamada"];
        }

        Model_Inbox::updateTicketLlamada($noLlamada, $noTicket);
        $this->redirect('/admin/index/llamada/llamada/'.$noLlamada.'/ticket/'.$noTicket);
    }
    public function cotizacionesAction()
    {

    }
    public function activosAction()
    {
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $this->_helper->viewRenderer->setNoRender(true);
        $Tickets= Model_Inbox::getActivos();

        $data = array();
    foreach ($Tickets as $cat) {

       $idTicket= $cat["idticket"];
       $Nombres= $cat["nombres"];
       $Apellidos= $cat["apellidos"];
       $Fecha= $cat["fecha"];
        $Comentarios= $cat["comentarios"];
        $Llamada= $cat["idllamada"];

        $data[$idTicket]['Ticket']=$idTicket;
        $data[$idTicket]['Element']='<a href="/admin/index/finalizar/ticket/'.$idTicket.'" class="list-group-item" id="T'.$idTicket.'">

                <label>
                    <input type="checkbox" name="'.$idTicket.'">
                </label>

            <span class="glyphicon glyphicon-star-empty"></span>
            <span class="name" style="min-width: 120px; display: inline-block;">'.$idTicket.'</span>
            <span class="">'.$Nombres.' '.$Apellidos.'</span>
            <span class="text-muted" style="font-size: 11px;">- '.$Comentarios.'</span>
            <span class="badge">'.$Fecha.'</span>
            <span class="pull-right">
                <span class="glyphicon glyphicon-paperclip"></span>
            </span>
        </a>';
    }

        echo json_encode($data);
    }
    public function inactivosAction()
    {
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $this->_helper->viewRenderer->setNoRender(true);
        $Tickets= Model_Inbox::getInactivos();

        $data = array();
        foreach ($Tickets as $cat) {
            $idTicket= $cat["idticket"];
            $data[$idTicket]['Ticket']=$idTicket;
        }

        echo json_encode($data);
    }

    public function guardarFormularioAction(){
        $idLlamada= $this->getRequest()->getParam('Llamada')  ;
        $Nombres= $this->getRequest()->getParam('nombres')  ;
        $Apellidos= $this->getRequest()->getParam('apellidos')  ;

        $Ticket= $this->getRequest()->getParam('Ticket')  ;
        $Fecha= $this->getRequest()->getParam('fecha')  ;

        $Agente= $this->getRequest()->getParam('agente')  ;
        $Email= $this->getRequest()->getParam('email')  ;

        $Checkin= $this->getRequest()->getParam('checkin')  ;
        $Checkout= $this->getRequest()->getParam('ckeckout')  ;

        $Reservacion2= $this->getRequest()->getParam('reservacion2')  ;
        $Promocion= $this->getRequest()->getParam('promocion')  ;

        $Hotel= $this->getRequest()->getParam('hotel')  ;
        $Medio= $this->getRequest()->getParam('medio')  ;

        $Adultos= $this->getRequest()->getParam('adultos')  ;
        $Ninos= $this->getRequest()->getParam('ninos')  ;

        $News= $this->getRequest()->getParam('informacion')  ;
        $Presupuesto= $this->getRequest()->getParam('presupuesto')  ;


        $Segmentos= $this->getRequest()->getParam('segmento')  ;
        $Comentarios= $this->getRequest()->getParam('comentarios')  ;
        $tipollamda= $this->getRequest()->getParam('llamada')  ;
        if($Checkin==""){$Checkin=null;}
        if($Checkout==""){$Checkout=null;}
        if($Adultos==""){$Adultos=null;}
        if($Ninos==""){$Ninos=null;}
        if($Presupuesto==""){$Presupuesto=null;}
        $this->_helper->viewRenderer->setNoRender(true);
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");

        //
        if($this->getRequest()->getParam('llamada')!==null){
            Model_Inbox::updateLlamada($idLlamada,$Nombres,$Apellidos,$Ticket,$Agente,$Email,$Checkin,$Checkout,$Reservacion2,$Promocion,$Hotel,$Medio,$Adultos, $Ninos, $News, $Presupuesto, $Segmentos, $Comentarios);
            Model_Inbox::updateTicketEstado(2,$Ticket);
            $this->redirect('/admin/index/dashboard');
        }
    }

    public function llamadaSimpleAction(){
        $idLlamada= $this->getRequest()->getParam('Llamada')  ;
        $Nombres= $this->getRequest()->getParam('nombres')  ;
        $Apellidos= $this->getRequest()->getParam('apellidos')  ;
        $Ticket= $this->getRequest()->getParam('Ticket')  ;
        $Fecha= $this->getRequest()->getParam('fecha')  ;
        $Agente= $this->getRequest()->getParam('agente')  ;
        $Email= $this->getRequest()->getParam('email')  ;
        $Checkin= $this->getRequest()->getParam('checkin')  ;
        $Checkout= $this->getRequest()->getParam('ckeckout')  ;
        $Reservacion2= $this->getRequest()->getParam('reservacion2')  ;
        $Promocion= $this->getRequest()->getParam('promocion')  ;
        $Hotel= $this->getRequest()->getParam('hotel')  ;
        $Medio= $this->getRequest()->getParam('medio')  ;
        $Adultos= $this->getRequest()->getParam('adultos')  ;
        $Ninos= $this->getRequest()->getParam('ninos')  ;
        $News= $this->getRequest()->getParam('informacion')  ;
        $Presupuesto= $this->getRequest()->getParam('presupuesto')  ;
        $Segmentos= $this->getRequest()->getParam('segmento')  ;
        $Comentarios= $this->getRequest()->getParam('comentarios')  ;
        $this->_helper->viewRenderer->setNoRender(true);
        if($Checkin==""){$Checkin=null;}
        if($Checkout==""){$Checkout=null;}
        if($Adultos==""){$Adultos=null;}
        if($Ninos==""){$Ninos=null;}
        if($Presupuesto==""){$Presupuesto=null;}
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        Model_Inbox::updateLlamada($idLlamada,$Nombres,$Apellidos,$Ticket,$Agente,$Email,$Checkin,$Checkout,$Reservacion2,$Promocion,$Hotel,$Medio,$Adultos, $Ninos, $News, $Presupuesto, $Segmentos, $Comentarios);
        Model_Inbox::updateTicketEstado(1,$Ticket);
        $this->redirect('/admin/index/llamada');

    }

    public function borrarPendienteAction(){
        include_once (APPLICATION_PATH .  "/modules/admin/models/Inbox.php");
        $Ticket= $this->getRequest()->getParam('ticket');
        Model_Inbox::updateTicketEstado(3,$Ticket);
        $this->_helper->viewRenderer->setNoRender(true);
    }
}

