<?php

class Model_Inbox{

    public static function getActivos() {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "SELECT a.idTicket, b.nombres, b.apellidos, b.idllamada,b.comentarios,  a.fecha FROM Ticket a, Llamada b where estado=1 and a.llamada = b.idllamada ";
            $result = $db->fetchAll($sql);
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }

    public static function getFinalizar( $ticket) {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "SELECT a.idticket, b.nombres, b.apellidos, b.idllamada, a.agente, b.email, a.fecha FROM Ticket a, Llamada b where estado=1 and a.llamada=b.idllamada and a.idticket= ".$ticket;
            $result = $db->fetchAll($sql);
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }
    public static function getInactivos() {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "SELECT idTicket FROM Ticket where estado=2 or estado=3";
            $result = $db->fetchAll($sql);
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }

    public static function updateLlamada($idLlamada, $nombre, $apellido, $ticket,  $agente, $email, $chekin, $checkout, $reservacion2, $promocion, $hotel, $medio, $adultos, $ninos, $news, $presupuesto, $segmento, $comentarios) {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "UPDATE llamada
   SET nombres=?, apellidos=?, noticket=?, agente=?,
       email=?, checkin=?, checkout=?, reservacion2=?, promocion=?,
       hotel=?, medio=?, adultos=?, ninos=?, newsletter=?, presupuesto=?,
       segmento=?, comentarios=?
       WHERE idLlamada=$idLlamada;

";
            $result = $db->fetchRow($sql, array($nombre, $apellido, $ticket, $agente, $email, $chekin, $checkout, $reservacion2, $promocion, $hotel, $medio, $adultos, $ninos, $news, $presupuesto, $segmento, $comentarios));
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }

    public static function setTicket() {

    try {
        $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
            'host'     => '127.0.0.1',
            'username' => 'postgres',
            'password' => 'postgres',
            'dbname'   => 'Sistema'
        ));
        $sql = "INSERT INTO ticket(
            idticket, agente, noticket, fecha, llamada, cotizacion, contrato,
            estado)
            VALUES (nextval('Ticket_Seq'),'Telefono1','00','".date("Y-m-d H:i:s")."',0,-1,-1,0);";
        $result = $db->fetchRow($sql);

        $sql = "select Ticket_Seq.last_value as noTicket from ticket_seq";
        $result = $db->fetchAll($sql);

    }catch (Zend_Db_Exception $e){
        $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
    }
    return $result;

}

    public static function setLlamada($ticket) {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "INSERT INTO llamada(
            idllamada, Fecha, noticket)
            VALUES (nextval('Llamada_Seq'),?,?);";
            $result = $db->fetchRow($sql, array(date("Y-m-d"),$ticket));

            $sql = "select Llamada_Seq.last_value as noLlamada from llamada_seq";
            $result = $db->fetchAll($sql);

        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }

    public static function updateTicketLlamada($llamda, $ticket) {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "Update ticket set Llamada = ? where idTicket = ?;";
            $result = $db->fetchRow($sql, array($llamda, $ticket));
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }
    public static function updateTicketEstado($estado, $ticket) {

        try {
            $db = new Zend_Db_Adapter_Pdo_Pgsql(array(
                'host'     => '127.0.0.1',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'Sistema'
            ));
            $sql = "Update ticket set estado = ? where idTicket = ?;";
            $result = $db->fetchRow($sql, array($estado, $ticket));
        }catch (Zend_Db_Exception $e){
            $result['error'] = array( 'iserror' => 1, 'message'=>$e->getMessage());
        }
        return $result;

    }
}