<?php

class Login_IndexController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function loginAction()
    {
        include_once (APPLICATION_PATH .  "/modules/login/models/Login.php");
        $User= $this->getRequest()->getParam('username')  ;
        $Pass= $this->getRequest()->getParam('password')  ;
        $Sesion= Model_Login::getSesion($User,$Pass);
        $Cont=0;
        $rol=0;
        $nombre='';
        foreach ($Sesion as $cat) {
            $Cont=$Cont+1;
            $rol=$cat["rol"];
            $nombre=$cat["nombre"];
        }
        if($Cont!=1){
            $this->view->message='Datos incorrectos';
            $this->_helper->viewRenderer('index');
        }
        else{
            $mysession = new Zend_Session_Namespace('mysession');
            $mysession->Nombre = $nombre;
            $mysession->Rol = $rol;
            $this->redirect('/admin/index/dashboard');

        }

    }
    public function indexAction()
    {


    }



}


